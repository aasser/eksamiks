package esimene;

public class Esimene {

	public static void main(String[] args) {
		System.out.println(arvutaSumma(5, 10));
		System.out.println(arvutaSumma(-5, 89));
		System.out.println(arvutaSumma(-60, -10));
	}

	public static int arvutaSumma(int a, int b) {

		int sum = a + b;

		return sum;

	}

}
